describe('Testing Object.assign()', () => {
  it('Working capacity', () => {
    const object1 = { z: 'z', y: 'y' };
    const object2 = { z: 'Z', x: 'x' };
    const result1 = Object.assign(object1, object2);
    const result2 = Object.assign(object2, object1);
    const mockResult = { z: 'Z', x: 'x', y: 'y' };

    expect.hasAssertions();

    // BEGIN
    expect(result1).toEqual(mockResult);
    expect(result2).toEqual(mockResult);
    // END
  });

  it('Change target object', () => {
    const object1 = { z: 'z', y: 'y' };
    const object2 = { z: 'Z', x: 'x' };
    const result = Object.assign(object1, object2);

    // BEGIN
    expect(object1).toEqual(result);
    // END
  });

  it('Copying symbolic properties', () => {
    const object1 = { x: 'x' };
    const object2 = { [Symbol('y')]: 'y' };
    const result = Object.assign(object1, object2);
    const mockResult = { x: 'x', [Symbol('y')]: 'y' };

    // BEGIN
    expect(result).toMatchObject(mockResult);
    // END
  });

  it('Inherited and non-enumerable properties are not copied', () => {
    const object = Object.create({ x: 'x' }, {
      y: {
        value: 'y',
      },
      z: {
        value: 'z',
        enumerable: true,
      },
    });
    const result = Object.assign(object);
    const mockResult = { z: 'z' };

    // BEGIN
    expect(result).toEqual(mockResult);
    // END
  });

  it('Primitives are wrapped in objects', () => {
    const object1 = {};
    const object2 = 'string';
    const object3 = true;
    const object4 = 10;
    const object5 = Symbol('x');
    const result = Object.assign(object1, object2, object3, object4, object5, undefined, null);
    // Only the wrapper over the string has its own enumerated properties
    const mockResult = {
      0: 's', 1: 't', 2: 'r', 3: 'i', 4: 'n', 5: 'g',
    };

    // BEGIN
    expect(result).toEqual(mockResult);
    // END
  });

  it('An exception interrupts the current copying task', () => {
    const object1 = Object.defineProperty({}, 'x', {
      value: 'z',
      writable: false,
    });
    const object2 = { x: 'X', y: 'y' };

    // BEGIN
    expect(() => Object.assign(object1, object2)).toThrow();
    // END
  });

  it('Copying methods', () => {
    const object1 = {
      x: 'x',
      get y() {
        return 'y';
      },
    };
    const result = Object.assign(object1);
    const mockResult = { x: 'x', y: 'y' };

    // BEGIN
    expect(result).toEqual(mockResult);
    // END
  });
});
