const fc = require('fast-check');

const sort = (data) => data.slice().sort((a, b) => a - b);
const analogSort = (data) => data.slice().sort((a, b) => a > b);

// BEGIN
describe('Testing Sort function from array', () => {
  it('Main work, just sort', () => {
    fc.assert(
      fc.property(
        fc.int8Array(),
        (data) => {
          const sortData = sort(data);
          expect(sortData).toBeSorted();
        },
      ),
    );
  });

  it('Sort reversibility', () => {
    fc.assert(
      fc.property(
        fc.int16Array(),
        (data) => {
          const sortData = sort(data);
          const firstReverse = sortData.reverse();
          expect(firstReverse).toBeSorted({ descending: true });
          const secondReverse = sortData.reverse();
          expect(secondReverse).toBeSorted({ descending: false });
        },
      ),
    );
  });

  it('Sort idempotency', () => {
    fc.assert(
      fc.property(
        fc.int32Array(),
        (data) => {
          const sortData = sort(data);
          const secondSortData = sort(sortData);
          expect(sortData).toBeSorted();
          expect(secondSortData).toBeSorted();
          expect(sortData).toEqual(secondSortData);
        },
      ),
    );
  });

  it('Unchanged length when sorting', () => {
    fc.property(
      fc.array(fc.integer()),
      (data) => {
        const sortData = sort(data);
        const dataLength = data.length;
        const sortDataLength = sortData.length;
        expect(dataLength).toEqual(sortDataLength);
      },
    );
  });

  it('Immutability of data when sorting', () => {
    fc.property(
      fc.array(fc.double()),
      (data) => {
        const sortData = sort(data);
        data.forEach((el) => expect(sortData).toContain(el));
      },
    );
  });

  it('Oracle test', () => {
    fc.assert(
      fc.property(
        fc.int32Array(),
        (data) => {
          const sortData = sort(data);
          const analogSortData = analogSort(data);
          expect(sortData).toEqual(analogSortData);
        },
      ),
    );
  });
});
// END
