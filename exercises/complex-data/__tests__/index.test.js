const faker = require('faker');

// BEGIN
describe('Testing Faker', () => {
  it('Matching basic types', () => {
    const transaction = faker.helpers.createTransaction();
    const expected = {
      amount: expect.any(String),
      date: expect.any(Date),
      business: expect.any(String),
      name: expect.any(String),
      type: expect.any(String),
      account: expect.any(String),
    };

    expect(transaction).toStrictEqual(expected);
  });

  test('Lack of invalid data types', () => {
    const transaction = faker.helpers.createTransaction();
    Object.keys(transaction).forEach((key) => {
      expect(transaction[key]).not.toBeNaN();
      expect(transaction[key]).not.toBeNull();
      expect(transaction[key]).not.toBeUndefined();
    });
  });

  it('Check for uniqueness of created objects', () => {
    const clearArray = Array.from(Array(500).keys());
    const transactions = clearArray.map(() => faker.helpers.createTransaction());
    const expectedTransaction = faker.helpers.createTransaction();

    expect(transactions).toMatchObject(
      expect.not.arrayContaining([
        expect.objectContaining(expectedTransaction),
      ]),
    );
  });
});
// END
